/**
 * Created by daniel on 6/12/16.
 */
// $(function() {
//     $("#tagsName").autocomplete({
//         source : function(request, response) {
//             $.getJSON("/", {
//                 term : request.term
//             }, response);
//         }
//     });
// });

$(document).ready(function() {
    $(function() {
        $("#tagsName").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/",
                    type: "POST",
                    data: {
                        term: request.term
                    },

                    dataType: "json",

                    success: function(data) {
                        response($.map(data, function(v,i){
                            return {
                                value: v.name
                            };
                        }));
                    }
                });
            }
        });
    });
});