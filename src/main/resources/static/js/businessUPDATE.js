/**
 * Created by daniel on 6/10/16.
 */

$('document').ready(function() {
    var avaResponse;
    $.ajax({
        url: '/api/users/business' + $('#userid').val(),
        type: 'GET',
        success: function (data) {
            console.log(data);
            $('#inputName').val(data.entity.name);
            $('#inputSurname').val(data.entity.surname);
            $('#inputEmail').val(data.entity.email);
            $('#inputCompany').val(data.entity.company);
            // if (data.entity.avatar!= null && data.entity.avatar.path != null) {
            //     avaResponse = data.entity.avatar.path;
            //     $('#avatar').attr('src', avaResponse);
            // }
        }
    });

    $('#btnSub').click(function() {
        $.ajax({
            url: '/api/users/'+ $('#userid').val(),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                'name' : $('#inputName').val(),
                'surname' : $('#inputSurname').val(),
                'email' : $('#inputEmail').val(),
                'company' : $('#inputCompany').val()
            }),
            success: function(data){
                console.log(data);
                window.location = '/business/account'
            },
            error: function(data){
                console.log(data);
            }

        })
    });

    // $('#avatar-input').on('change', function() {
    //     var formData = new FormData();
    //     formData.append('file', $('input[type=file]')[0].files[0]);
    //     console.log("form data: " + formData);
    //     $.ajax({
    //         url : '/api/file/upload',
    //         data : formData,
    //         processData : false,
    //         contentType : false,
    //         type : 'POST',
    //         success : function(data) {
    //             console.log("data:" + data);
    //             avaResponse = data;
    //             $('#avatar').attr('src', avaResponse);
    //         },
    //         error : function(err) {
    //             console.log(err);
    //         }
    //     });
    // })

});