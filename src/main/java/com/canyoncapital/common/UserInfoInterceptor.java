package com.canyoncapital.common;

import com.canyoncapital.service.UserAuthenticationService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author daniel
 */
public class UserInfoInterceptor extends HandlerInterceptorAdapter {

    private UserAuthenticationService authenticationService;

    public UserInfoInterceptor(UserAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            modelAndView.addObject("loggedInUserId", authenticationService.getLoggedInUserId());
        }
    }
}