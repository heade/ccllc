package com.canyoncapital.validator;

import com.canyoncapital.dto.BusinessDto;
import com.canyoncapital.dto.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author daniel
 */
@Component("userDTOValidator")
public class UserDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDto userDto = (BusinessDto)target;
        if(userDto.getEmail() == null || userDto.getEmail().isEmpty()){
            errors.rejectValue("bad_email", "email must not be empty");
        }

        if(userDto.getPassword() == null || userDto.getPassword().length() < 5){
            errors.rejectValue("bad_password", "password size must be > 5");
        }
    }
}
