package com.canyoncapital.validator;

import com.canyoncapital.dto.ForgotPasswordDto;
import com.canyoncapital.dto.PasswordDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author daniel
 */
@Component("forgotPasswordDTOValidator")
public class ForgotPasswordDTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return ForgotPasswordDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ForgotPasswordDto forgotPasswordDto = (ForgotPasswordDto) o;
        if (forgotPasswordDto.getNewPassword() == null || forgotPasswordDto.getNewPassword().length() < 5) {
            errors.rejectValue("bad_password", "password size must be > 5");
        }

    }
}
