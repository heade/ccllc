package com.canyoncapital.validator;

import com.canyoncapital.dto.PasswordDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author daniel
 */
@Component("passwordDTOValidator")
public class PasswordDTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PasswordDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PasswordDto passwordDto = (PasswordDto) o;
        if (passwordDto.getNewPassword() == null || passwordDto.getNewPassword().length() < 5) {
            errors.rejectValue("bad_password", "password size must be > 5");
        }

    }
}
