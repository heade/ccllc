package com.canyoncapital.service;

import com.canyoncapital.dao.UserDao;
import com.canyoncapital.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author daniel
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    @Override
    public <T extends User> T read(Class<T> tClass, Long id) {
        return userDao.read(tClass, id);
    }

    @Transactional(readOnly = true)
    @Override
    public User read(Long id) {
        return userDao.read(id);
    }

    @Transactional(readOnly = true)
    @Override
    public <T extends User> T readByEmail(Class<T> tClass, String email) {
        return userDao.readByEmail(tClass, email);
    }

    @Transactional(readOnly = true)
    @Override
    public User readByEmail(String email) {
        return userDao.readByEmail(email);
    }

    @Transactional
    @Override
    public <T extends User> T readByActivationKey(Class<T> tClass, String activationKey) {
        return userDao.readByActivationKey(tClass, activationKey);
    }

    @Transactional
    @Override
    public <T extends User> T readByPasswordKey(Class<T> tClass, String passwordKey) {
        return userDao.readByPasswordKey(tClass, passwordKey);
    }

    @Transactional
    @Override
    public <T extends User> T create(T object) {
        object.setPassword(passwordEncoder.encode(object.getPassword()));
        return userDao.create(object);
    }

    @Transactional
    @Override
    public <T extends User> T update(T object) {
        return userDao.update(object);
    }

    @Transactional
    @Override
    public <T extends User> void delete(T object) {
        userDao.delete(object);
    }

    @Override
    public Long getIdByEmail(String email) {
        return userDao.getIdByEmail(email);
    }

    @Override
    public List<User> getList() {
        return userDao.getList();
    }
}
