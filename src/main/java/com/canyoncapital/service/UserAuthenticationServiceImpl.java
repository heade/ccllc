package com.canyoncapital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * @author daniel
 */
@Service
public class UserAuthenticationServiceImpl implements UserAuthenticationService {

    @Autowired
    private UserService userService;

    @Override
    public Long getLoggedInUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal instanceof User) {
                User userDetails = (User) principal;
                return userService.readByEmail(com.canyoncapital.entity.User.class, userDetails.getUsername()).getId();
            }
        }
        return null;
    }
//    @Override
//    public Long getLoggedInCustomerId() {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (auth != null) {
//            Object principal = auth.getPrincipal();
//            if (principal instanceof User) {
//                User userDetails = (Individual) principal;
//                return customerService.getIdByEmail(userDetails.getUsername());
//            }
//        }
//        return null;
//    }
}
