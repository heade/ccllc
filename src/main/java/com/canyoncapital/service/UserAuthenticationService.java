package com.canyoncapital.service;

/**
 * @author daniel
 */
public interface UserAuthenticationService {
    Long getLoggedInUserId();
}
