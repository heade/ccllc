package com.canyoncapital.service;

import com.canyoncapital.entity.Business;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * @author daniel
 */
@Service("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService{

    @Autowired
    private UserService userService;

    @SuppressWarnings("unchecked")
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.canyoncapital.entity.User user = userService.readByEmail(username);
        if(user == null){
            throw new UsernameNotFoundException("User not found");
        }
        List<GrantedAuthority> authorities = (List<GrantedAuthority>) getAuthorities(user);
        return createUser(user, authorities);
    }

    public void signin(com.canyoncapital.entity.User user){
        if(user != null){
            UserDetails userDetails = loadUserByUsername(user.getEmail());
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }
    private User createUser(com.canyoncapital.entity.User user, List<GrantedAuthority> authorities) {
        return new User(user.getEmail(), user.getPassword(), authorities);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(com.canyoncapital.entity.User user) {
        return AuthorityUtils.createAuthorityList(user.getRole().name());
    }
}
