//package com.canyoncapital.service;
//
//import com.canyoncapital.entity.User;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Collections;
//import java.util.List;
//
///**
// * @author daniel
// */
//@Service("userDetailsService")
//public class MyUserDetailService implements UserDetailsService {
//
//    @Autowired
//    private UserService userService;
//
//    @Transactional(readOnly = true)
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        User user = userService.readByEmail(User.class, email);
//        if (user == null) {
//            throw new UsernameNotFoundException("User not found");
//        }
//        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getGrantedAuthority(user));
//    }
//
//    private List<GrantedAuthority> getGrantedAuthority(User user) {
//        return Collections.singletonList(new SimpleGrantedAuthority(user.getRole().name()));
//    }
//}
