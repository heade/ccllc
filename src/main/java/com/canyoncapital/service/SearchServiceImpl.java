package com.canyoncapital.service;

import com.canyoncapital.dao.UserDao;
import com.canyoncapital.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
/**
 * @author daniel
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private UserService userService;

    @Override
    public List<User> searchResult(String name){
        List<User> result = new ArrayList<>();
        List<User> userList = userService.getList();

        for (User tag:userList){
            if(tag.getName().contains(name)){
                result.add(tag);
            }
        }
        return result;
    }
}
