package com.canyoncapital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * @author daniel
 */
@Component
@Scope("singleton")
@PropertySource("classpath:application.properties")
public class MailService {

//    @Autowired
//    private MailSender mailSender;
//
//    @Async
//    public void sendMail(String to, String subject, String body){
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setTo(to);
//        message.setSubject(subject);
//        message.setText(body);
//        mailSender.send(message);
//    }

    @Value("${mail.username}")
    private String mailUsername;

    @Value("${mail.password}")
    private String mailPassword;

    @Value("${mail.host}")
    private String host;

    @Value("${mail.port}")
    private String port;

    @Autowired
    private Session mailSession;

    public void sendMail(String toAddress, String subject, String message) throws AddressException,
            MessagingException {

        // creates a new e-mail message
        Message msg = new MimeMessage(mailSession);

        msg.setFrom(new InternetAddress(mailUsername));
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        // set plain text message
        msg.setContent(message, "text/html");

        // sends the e-mail
        Transport.send(msg);

    }

}
