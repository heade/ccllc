package com.canyoncapital.service;

import com.canyoncapital.entity.User;

import java.util.List;

/**
 * @author daniel
 */
public interface SearchService{
    List<User> searchResult(String name);
}

