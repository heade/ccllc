package com.canyoncapital.service;

import com.canyoncapital.entity.User;

import java.util.List;

/**
 * @author daniel
 */
public interface UserService {
    <T extends User> T read(Class<T> tClass, Long id);
    User read(Long id);
    <T extends User> T readByEmail(Class<T> tClass, String email);
    User readByEmail(String email);
    <T extends User> T readByActivationKey(Class<T> tClass, String activationKey);
    <T extends User> T readByPasswordKey(Class<T> tClass, String passwordKey);
    <T extends User> T create(T object);
    <T extends User> T update(T object);
    <T extends User> void delete(T object);
    Long getIdByEmail(String email);
    List<User> getList();
}
