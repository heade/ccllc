package com.canyoncapital.converter;

import com.canyoncapital.dto.UserDto;
import com.canyoncapital.entity.User;
import com.canyoncapital.service.UserService;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

/**
 * @author daniel
 */
public class UserDtoToUserConverter implements Converter<UserDto, User> {

    private UserService userService;
    private ConversionService conversionService;

    public UserDtoToUserConverter(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @Override
    public User convert(UserDto userDto) {
        User user = null;
        Long id = userDto.getId();
        if(id != null){
            user = userService.read(id);
        }

        if(user==null){
            user = new User();
        }
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setPassword(userDto.getPassword());
        user.setCountry(userDto.getCountry());
        user.setCity(userDto.getCity());
        user.setState(userDto.getState());
        user.setZip(userDto.getZip());
        user.setStreetAddress(userDto.getStreetAddress());
        user.setTelephone(userDto.getTelephone());
        return user;
    }
}
