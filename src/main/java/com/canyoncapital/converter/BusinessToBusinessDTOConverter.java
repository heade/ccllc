package com.canyoncapital.converter;


import com.canyoncapital.dto.BusinessDto;
import com.canyoncapital.entity.Business;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

/**
 * @author daniel
 */
public class BusinessToBusinessDTOConverter implements Converter<Business,BusinessDto> {

    private ConversionService conversionService;

    @Override
    public BusinessDto convert(Business business) {
        if( business == null) return null;

        BusinessDto businessDto = new BusinessDto();
        businessDto.setName(business.getName());
        businessDto.setSurname(business.getSurname());
        businessDto.setCompany(business.getMerchantCompany());
        businessDto.setEmail(business.getEmail());
        return businessDto;
    }

    public BusinessToBusinessDTOConverter(ConversionService conversionService) {
        this.conversionService = conversionService;

    }


}
