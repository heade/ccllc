package com.canyoncapital.converter;

import com.canyoncapital.dto.BusinessDto;
import com.canyoncapital.entity.Business;
import com.canyoncapital.entity.Roles;
import com.canyoncapital.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

/**
 * @author daniel
 */
public class BusinessDtoToBusiness implements Converter<BusinessDto, Business> {

    private UserService userService;
    private ConversionService conversionService;

    public BusinessDtoToBusiness(UserService userService, ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @Override
    public Business convert(BusinessDto businessDto) {
        Business business = null;// = null;
        //Business business = new Business();
        Long id = businessDto.getId();
        System.out.println("id: "+ id);
        if (id != null){
            business = userService.read(Business.class, id);
        }

        if ( business == null){
            business = new Business();
            business.setRole(Roles.ROLE_UNACTIVE);
        }
        business.setName(businessDto.getName());
        business.setSurname(businessDto.getSurname());
        business.setEmail(businessDto.getEmail());
        business.setMerchantCompany(businessDto.getCompany());
        return business;
    }
}
