package com.canyoncapital.converter;

import com.canyoncapital.service.MailService;
import com.canyoncapital.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author daniel
 */
@Service("conversionService")
public class CanyonCapitalConversionServiceFactoryBean extends ConversionServiceFactoryBean {

    @Autowired
    @Lazy
    private UserService userService;

    @Autowired
    @Lazy
    private MailService mailService;

    public CanyonCapitalConversionServiceFactoryBean() {
        Set<Converter> converts = new HashSet<>();

    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        ConversionService conversionService = getObject();
        ConverterRegistry registry = (ConverterRegistry) conversionService;
        registry.addConverter(new BusinessToBusinessDTOConverter(conversionService));
        registry.addConverter(new BusinessDtoToBusiness(userService, conversionService));
    }
}
