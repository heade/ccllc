package com.canyoncapital.support;

import java.io.Serializable;

/**
 * @author daniel
 */
public class Message implements Serializable {

    public static final String MESSAGE_LIST = "messageList";
    public static final String MESSAGE_ATTRIBUTE = "message";

    public static enum Type{
        DANGER, INFO, WARNING, SUCCESS;
    }

    private final Type type;
    private final Object[] args;
    private final String message;

    public Message(String message, Type type) {
        this.message = message;
        this.type = type;
        this.args = null;
    }

    public Message(String message, Type type, Object... args) {
        this.type = type;
        this.args = args;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getMessage() {
        return message;
    }
}
