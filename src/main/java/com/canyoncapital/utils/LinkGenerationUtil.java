package com.canyoncapital.utils;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;

/**
 * @author daniel
 */
public class LinkGenerationUtil {
    public static String getActivationLink(HttpServletRequest req, String activationKey){
        String result = "";
        try{
            URL url = new URL(req.getRequestURL().toString());
            result += "http://";
            result += url.getHost() + ":";
            result += url.getPort();
        } catch (Exception e){
            e.printStackTrace();
        }
        result += "/activate/" + activationKey;
        return result;
    }
}
