package com.canyoncapital.utils;

import java.util.Random;

/**
 * @author daniel
 */
public class KeyGenerationUtil {
//
//    private static String charsActivtionKey = "abcdefghijklmnopqrstuvwxy1234567890";
//    private static String charsPasswordKey = "abcdefghijklmnopqrstuvwxy1234567890";

    public static String getActivtionKey(String key, int size) {
        String result = "";
        char[] chars = key.toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        result += sb.toString();
        return result;
    }
//    public static String getKey() {
//        String result = "";
//        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
//        StringBuilder sb = new StringBuilder();
//        Random random = new Random();
//        for (int i = 0; i < 20; i++) {
//            char c = chars[random.nextInt(chars.length)];
//            sb.append(c);
//        }
//        result += sb.toString();
//        return result;
//    }
}
