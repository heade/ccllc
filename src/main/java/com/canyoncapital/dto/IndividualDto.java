package com.canyoncapital.dto;

import java.util.Calendar;

/**
 * @author daniel
 */
public class IndividualDto extends UserDto{

    private Calendar birthDate;
    private int age;

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
