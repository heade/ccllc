package com.canyoncapital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaytheosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaytheosApplication.class, args);
	}
}
