package com.canyoncapital.web;

import com.canyoncapital.dto.UserDto;
import com.canyoncapital.entity.Business;
import com.canyoncapital.service.UserAuthenticationService;
import com.canyoncapital.service.UserService;
import com.canyoncapital.support.MessageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @author daniel
 */
@Controller
public class UserController {
    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/SignUpForBusiness", method = RequestMethod.GET)
    public String signUpMerchantPage(Model model) {
        model.addAttribute("message", "create");
        return "SignUpForBusiness";
    }

//    @RequestMapping(value = "/SignUpForBusiness", method = RequestMethod.GET)
//    public String signUpMerchantPage(Model model) {
//        model.addAttribute("message", "create");
//        return "SignUpForBusiness/SignUpForBusiness";
//    }

//    @RequestMapping(value = "/", method = RequestMethod.POST)
//    public String signUpMerchantPage(Model model, Errors errors) {
//        if (errors.hasErrors()) {
//            return "cc-homepage";
//        }
//        if(userService.readByEmail(Business.class, "danildubinin2@gmail.com") != null) {
//            MessageHelper.addErrorAttribute(model, "signup.error");
//            return "SignUpForBusiness";
//        }
//        model.addAttribute("message", "create");
//        return "redirect:/";
//    }

    @RequestMapping(value = "/business/{id}", method = RequestMethod.GET)
    public String getById(@PathVariable(value = "id") Long id, Model model) {
        model.addAttribute("userid", id);
        return "user";
    }
    @RequestMapping(value = "/business/account", method = RequestMethod.GET)
    public String getUser(Model model){
        Long id = userAuthenticationService.getLoggedInUserId();
        model.addAttribute("userid", id);
        return "ProfileBusiness";
    }

    @RequestMapping(value = "/business/update/password", method = RequestMethod.GET)
    public String updatePassword(Model model){
//        Long id = userAuthenticationService.getLoggedInUserId();
//        model.addAttribute("userid", id);
//        model.addAttribute("message", "update");
//        return "updatePassword";
        model.addAttribute("message", "create");
        return "redirect:/";
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
    public String forgotPasswordEmail(Model model){
//        Long id = userAuthenticationService.getLoggedInUserId();
//        model.addAttribute("userid", id);
//        model.addAttribute("message", "update");
//        return "forgotPasswordEmail";
        model.addAttribute("message", "create");
        return "forgotPasswordEmail";
    }

    @RequestMapping(value = "/forgot/password", method = RequestMethod.GET)
    public String forgotPassword(Model model){
        Long id = userAuthenticationService.getLoggedInUserId();
        model.addAttribute("userid", id);
        model.addAttribute("message", "update");
        return "forgotPassword";
    }
    @RequestMapping(value = "/business/update", method = RequestMethod.GET)
    public String updateProfile(Model model){
        Long id = userAuthenticationService.getLoggedInUserId();
        model.addAttribute("userid", id);
        model.addAttribute("message", "update");
        return "updateProfile";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(HttpServletRequest request, @RequestParam(name="redirect", required = false) String redirect, Model model){
        request.getSession().setAttribute("url_redirect_login", redirect);
        model.addAttribute("userDto", new UserDto());
        return "index";
    }
}
