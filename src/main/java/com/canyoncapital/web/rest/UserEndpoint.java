package com.canyoncapital.web.rest;

import com.canyoncapital.dto.*;
import com.canyoncapital.entity.Business;
import com.canyoncapital.entity.Roles;
import com.canyoncapital.entity.User;
import com.canyoncapital.service.MyUserDetailsService;
import com.canyoncapital.service.MailService;
import com.canyoncapital.service.SearchService;
import com.canyoncapital.utils.KeyGenerationUtil;
import com.canyoncapital.utils.LinkGenerationUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import com.canyoncapital.service.UserService;
import com.canyoncapital.validator.UserDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author daniel
 */
@RestController
@RequestMapping(value = "/api/users")
public class UserEndpoint extends HttpServlet {



    @Autowired
    private UserDtoValidator userDtoValidator;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    @Qualifier("passwordDTOValidator")
    private Validator passwordDTOValidator;

    @Autowired
    @Qualifier("forgotPasswordDTOValidator")
    private Validator forgotPasswordDTOValidator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SearchService searchService;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@RequestBody UserDto userDto, BindingResult bindingResult, HttpServletRequest request) throws ServletException, IOException, AddressException, MessagingException {
       // userDtoValidator.validate(userDto, bindingResult);

//        if(bindingResult.hasErrors()){
//            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
//        }
//
//        Business business = conversionService.convert(businessDto, Business.class);
//        if (userService.getIdByEmail(business.getEmail()) != null) {
//            return ResponseEntity.badRequest().body(new BaseResponse());
//        }
//        business.setMerchant_company(businessDto.getCompany());
//        business = userService.create(business);
//        businessDto = conversionService.convert(business, BusinessDto.class);
//        return ResponseEntity.ok(new EntityResponse<>(businessDto));

          //todo User user = conversionService.convert(userDto, User.class);

         User user = new User(
                userDto.getEmail(), userDto.getName(), userDto.getSurname(), userDto.getPassword(), userDto.getCountry()
                ,userDto.getCity(), userDto.getState(), userDto.getZip(), userDto.getStreetAddress(), userDto.getTelephone());
        user.setRole(Roles.ROLE_UNACTIVE);
        //user.setMerchantCompany(businessDto.getCompany());
        user = userService.create(user);
        String activationLink = LinkGenerationUtil.getActivationLink(request, user.getActivationKey());
        mailService.sendMail(user.getEmail(), "Activation", "Dear " + user.getName() + ", confirm your account by clicking on the following link:  " +  activationLink);
        myUserDetailsService.signin(user);
        return ResponseEntity.ok(userDto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> read(@PathVariable("id") Long id){
        BusinessDto businessDto = conversionService.convert(userService.read(Business.class, id), BusinessDto.class);
        return ResponseEntity.ok(new EntityResponse<>(businessDto));
    }

    @RequestMapping(value = "/pass/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> updatePassword(@PathVariable("id") Long id, @RequestBody PasswordDto passwordDto, BindingResult bindingResult) throws AddressException, MessagingException{
        passwordDto.setId(id);

//        String message = "<h3 style='padding:5px;background:#6495ED;'>Greetings!</h3>";
//        message += "<b><h2 style='padding:20px;background:black;'>Wish you a nice day!</h2></b><br>";
//        message += "<font color=red>Daniel</font>";

        passwordDTOValidator.validate(passwordDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
        }
        User user = userService.read(id);
        if(passwordEncoder.matches(passwordDto.getOldPassword(), user.getPassword())){
            user.setPassword(passwordEncoder.encode(passwordDto.getNewPassword())); // else Individual individual = userService.read(Individual.class, id);
        }else{
            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
        }
        user = userService.update(user);
        UserDto userDto= conversionService.convert(user, BusinessDto.class);
        mailService.sendMail(user.getEmail(), "Password", "Your password was changed.");
        return ResponseEntity.ok(new EntityResponse<>(userDto));
    }

//    @RequestMapping(value = "forgotPassEmail", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public void forgotPassEmail(@RequestBody ForgotPasswordDto forgotPasswordDto, BindingResult bindingResult)throws AddressException, MessagingException{
//        String email = forgotPasswordDto.getEmail();
//        User user = userService.readByEmail(User.class, email);
////        List<User> users = userService.getList();
////        User user = new User();
////        String email = forgotPasswordDto.getEmail();
////        for (User u: users){
////            if (u.getEmail().equals(email)){
////                user = u;
////            }
////        }
//
//
//        user.setPasswordKey(KeyGenerationUtil.getActivtionKey("abcdefghijklmnopqrstuvwxyz1234567890", 6));
//        //System.out.println(user.getPasswordKey());
//        mailService.sendMail(email, "Forgot password", "Key: "+ user.getPasswordKey());
//    }

    @RequestMapping(value = "/forgotPass", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> forgotPassword(@RequestBody ForgotPasswordDto forgotPasswordDto, BindingResult bindingResult) throws AddressException, MessagingException{
        forgotPasswordDTOValidator.validate(forgotPasswordDto, bindingResult);
        if (bindingResult.hasErrors()){
            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
        }
        String email = forgotPasswordDto.getEmail();
        User user = userService.readByEmail(User.class, email);
        user.setPasswordKey(KeyGenerationUtil.getActivtionKey("abcdefghijklmnopqrstuvwxyz1234567890", 6));
        mailService.sendMail(email, "Forgot password", "Key: "+ user.getPasswordKey());
//        User user = userService.readByPasswordKey(User.class, forgotPasswordDto.getPasswordKey());
//        System.out.println(forgotPasswordDto.getPasswordKey());
        if (forgotPasswordDto.getPasswordKey().equals(user.getPasswordKey())){
            user.setPassword(passwordEncoder.encode(forgotPasswordDto.getNewPassword()));
        }else {
            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
        }
        user = userService.update(user);
        UserDto userDto = conversionService.convert(user, BusinessDto.class);
        mailService.sendMail(user.getEmail(), "Password", "Your password was changed.");
        return ResponseEntity.ok(new EntityResponse<>(userDto));
    }

    @RequestMapping(value = "/business/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> updateBusiness(@PathVariable("id") Long id, @RequestBody BusinessDto businessDto, BindingResult bindingResult) {
        businessDto.setId(id);
//        emailDTOValidator.validate(userDTO, bindingResult);
//        userIdValidator.validate(id, bindingResult);
//        if (bindingResult.hasErrors()) {
//            return ResponseEntity.badRequest().body(new BaseResponse(bindingResult));
//        }

        Business business = userService.read(Business.class, id);
//        business = conversionService.convert(businessDto, Business.class);
       // todo business = conversionService.convert(businessDto, Business.class);
        business.setName(businessDto.getName()); //that's work padebilnomu
        business.setSurname(businessDto.getSurname()); //so i need to change it
        business.setEmail(businessDto.getEmail());//all of this
        business.setMerchantCompany(businessDto.getCompany());//and this too...=)
        business = userService.update(business);
        businessDto = conversionService.convert(business, BusinessDto.class);
        return ResponseEntity.ok(new EntityResponse<>(businessDto));
    }

}
