package com.canyoncapital.web;

import com.canyoncapital.dto.UserDto;
import com.canyoncapital.entity.User;
import com.canyoncapital.service.SearchServiceImpl;
import com.canyoncapital.service.UserAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author daniel
 */
@Controller
public class MainController {

    @Autowired
    private UserAuthenticationService authenticationServicel;

    @Autowired
    private SearchServiceImpl searchService;

    @RequestMapping(value = {"/", "home"}, method = RequestMethod.GET)
    public String homePage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "home/index";
    }

//    @RequestMapping(value = {"/", "home"}, method = RequestMethod.GET)
//    public String homePage(HttpServletRequest request, @RequestParam(name="redirect", required = false) String redirect, Model model){
//        request.getSession().setAttribute("url_redirect_login", redirect);
//        model.addAttribute("userDto", new UserDto());
//        return "index";
//    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutPage(){return "about";}

    @RequestMapping(value = "/SignUp", method = RequestMethod.GET)
    public String signUp() {
        return "SignUp";
    }

    @RequestMapping(value = "/AboutUs", method = RequestMethod.GET)
    public String aboutUs() {
        return "AboutUs";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public @ResponseBody
    List<User> getSearchResult(@RequestParam("term") String name){
        List<User> names = searchService.searchResult(name);
        return names;
    }

//    @RequestMapping(value = "/autoComp", method = RequestMethod.GET)
//    public ModelAndView getPages() {
//        ModelAndView model = new ModelAndView("autoComp");
//        return model;
//    }
//
    @RequestMapping(value = "/getEmployees", method = RequestMethod.POST, produces = "application/json")
    public  @ResponseBody List<User> getEmployees(@RequestParam String term, HttpServletResponse response) {
        return searchService.searchResult(term);

    }

    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String accessDenied(){return "accessDenied";}
}
