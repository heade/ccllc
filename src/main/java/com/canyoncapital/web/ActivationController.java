package com.canyoncapital.web;

import com.canyoncapital.entity.Business;
import com.canyoncapital.entity.Roles;
import com.canyoncapital.service.MyUserDetailsService;
import com.canyoncapital.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author daniel
 */
@Controller
@RequestMapping(value = "activate")
public class ActivationController {

    @Autowired
    private UserService userService;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @RequestMapping(value = "/{activationKey}", method = RequestMethod.GET)
    public String activate(@PathVariable String activationKey, HttpServletRequest request, RedirectAttributes ra) throws ServletException, IOException {
        Business business = userService.readByActivationKey(Business.class, activationKey);
        if(business != null && business.getRole().equals(Roles.ROLE_UNACTIVE)){
            if(business.getActivationKey().equals(activationKey)){
                business.setRole(Roles.ROLE_BUSINESS);
                business = userService.update(business);
//                MyUserDetailsService myUserDetailsService = new MyUserDetailsService();
                myUserDetailsService.signin(business);
                //request.login(business.getEmail(), business.getPassword());
                return "redirect:/";
            }
        }
        return "redirect:/";
    }
}
