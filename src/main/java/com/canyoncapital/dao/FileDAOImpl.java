package com.canyoncapital.dao;

import com.canyoncapital.entity.FileEntity;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author daniel
 */
@Repository
public class FileDAOImpl implements FileDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public FileEntity create(FileEntity fileEntity) {
        entityManager.persist(fileEntity);
        return fileEntity;
    }

    @Override
    public FileEntity read(Long id) {return entityManager.find(FileEntity.class, id); }

    @Override
    public FileEntity update(FileEntity fileEntity) {
        return entityManager.merge(fileEntity);
    }

    @Override
    public void delete(FileEntity fileEntity) {
        entityManager.remove(fileEntity);
    }

    @Override
    public FileEntity read(String path) {
        return (FileEntity)entityManager.unwrap(Session.class).createCriteria(FileEntity.class)
                .add(Restrictions.eq("path", path))
                .uniqueResult();
    }
}
