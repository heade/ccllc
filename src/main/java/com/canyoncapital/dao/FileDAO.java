package com.canyoncapital.dao;

import com.canyoncapital.entity.FileEntity;

/**
 * @author daniel
 */
public interface FileDAO {
    FileEntity create(FileEntity fileEntity);
    FileEntity read(Long id);
    FileEntity update(FileEntity fileEntity);
    void delete(FileEntity fileEntity);
    FileEntity read(String path);
}
