package com.canyoncapital.dao;

import com.canyoncapital.entity.User;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author daniel
 */
@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public <T extends User> T read(Class<T> tClass, Long id) {
        return entityManager.find(tClass, id);
    }

    @Override
    public User read(Long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public <T extends User> T readByEmail(Class<T> tClass, String email) {
        return (T) entityManager.unwrap(Session.class)
                .createCriteria(tClass)
                .add(Restrictions.eq("email", email))
                .uniqueResult();
    }

    @Override
    public User readByEmail(String email) {
        return (User)entityManager.unwrap(Session.class)
                .createCriteria(User.class)
                .add(Restrictions.eq("email", email))
                .uniqueResult();
    }

    @Override
    public <T extends User> T readByActivationKey(Class<T> tClass, String activationKey) {
        return (T) entityManager.unwrap(Session.class)
                .createCriteria(tClass)
                .add(Restrictions.eq("activationKey", activationKey))
                .uniqueResult();
    }

    @Override
    public <T extends User> T readByPasswordKey(Class<T> tClass, String passwordKey) {
        return (T) entityManager.unwrap(Session.class)
                .createCriteria(tClass)
                .add(Restrictions.eq("passwordKey", passwordKey))
                .uniqueResult();
    }

    @Override
    public <T extends User> T create(T object) {
        entityManager.persist(object);
        return object;
    }

    @Override
    public <T extends User> T update(T object) {
        return entityManager.merge(object);
    }

    @Override
    public <T extends User> void delete(T object) {
        entityManager.remove(object);
    }

    @Override
    public Long getIdByEmail(String email) {
        return (Long) entityManager.unwrap(Session.class).createCriteria(User.class)
                .add(Restrictions.eq("email", email))
                .setProjection(Projections.property("id"))
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getList() {
        return ((Session) entityManager.getDelegate()).createCriteria(User.class)
                .list();
    }

}

