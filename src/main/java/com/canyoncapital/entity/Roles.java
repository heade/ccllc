package com.canyoncapital.entity;

import java.io.Serializable;

/**
 * @author daniel
 */
public enum Roles implements Serializable {
    ROLE_CUSTOMER,
    ROLE_BUSINESS,
    ROLE_UNACTIVE;

    public static Roles stringToEnum(String role) {
        for (Roles r : Roles.values()) {
            if (role.equals(r.name())) {
                return r;
            }
        }
        return null;
    }

}
