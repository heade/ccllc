package com.canyoncapital.entity;

/**
 * @author daniel
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

@Entity
@Table(name = "customer")
public class Individual extends User {

    @Column(name = "birthday_date")
    @NotNull
    private Calendar birthdayDate;

    @Column(name = "age")
    private int age;

    public Individual(){
        super();
    }

    public Individual(String email, String name, String surname, String password) {
        super(email, name, surname, password);
    }

    public Calendar getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Calendar birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
