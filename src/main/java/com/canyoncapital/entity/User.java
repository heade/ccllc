package com.canyoncapital.entity;

import com.canyoncapital.utils.KeyGenerationUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author daniel
 */
@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public class User {

    @Id
    @SequenceGenerator(name = "user_seq_gen", sequenceName = "user_seq", allocationSize = 1)
    @GeneratedValue(generator = "user_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "surname")
    @NotNull
    private String surname;

    @Column(name = "password")
    private String password;

    @Column(name ="country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private Long zip;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "charga_card_number")
    private String chargaCardNumber;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "registration_date")
    @Temporal(TemporalType.DATE)
    private Calendar registrationDate;

    @Column(name = "activationKey")
    private String activationKey;

    @Column(name = "passwordKey")
    private String passwordKey;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "file_id")
    private FileEntity avatar;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Roles role;

    public User() {
        role = Roles.ROLE_UNACTIVE;
        registrationDate = Calendar.getInstance();
        activationKey = KeyGenerationUtil.getActivtionKey("abcdefghijklmnopqrstuvwxy1234567890", 30);
//        passwordKey = KeyGenerationUtil.getActivtionKey("abcdefghijklmnopqrstuvwxy1234567890", 30);
    }

    public User(String email, String name, String surname, String password) {
        this();
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.password = password;
    }

    public User(String email, String name, String surname, String password, String country, String city, String state, Long zip, String streetAddress, String telephone) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.country = country;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.streetAddress = streetAddress;
        this.telephone = telephone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getZip() {
        return zip;
    }

    public void setZip(Long zip) {
        this.zip = zip;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getChargaCardNumber() {
        return chargaCardNumber;
    }

    public void setChargaCardNumber(String chargaCardNumber) {
        this.chargaCardNumber = chargaCardNumber;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Calendar getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPasswordKey() {
        return passwordKey;
    }

    public void setPasswordKey(String passwordKey) {
        this.passwordKey = passwordKey;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public FileEntity getAvatar() {
        return avatar;
    }

    public void setAvatar(FileEntity avatar) {
        this.avatar = avatar;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
