package com.canyoncapital.entity;

/**
 * @author daniel
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "merchant")
public class Business extends User {

    @Column(name = "merchant_company", unique = true)
    @NotNull
    private String merchantCompany;

    @Column(name = "business_type")
    private BusinessType businessType;

    @Column(name = "EIN")
    private String ein;

    public Business(){
        super();
    }

    public Business(String merchant_email, String merchant_name, String merchant_surname, String merchant_password){
        super(merchant_email, merchant_name, merchant_surname, merchant_password);
    }

    public String getMerchantCompany() {
        return merchantCompany;
    }

    public void setMerchantCompany(String merchantCompany) {
        this.merchantCompany = merchantCompany;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public String getEin() {
        return ein;
    }

    public void setEin(String ein) {
        this.ein = ein;
    }
}
